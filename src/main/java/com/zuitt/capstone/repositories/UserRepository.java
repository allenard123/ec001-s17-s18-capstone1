package com.zuitt.capstone.repositories;

import com.zuitt.capstone.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Object> {
    User findByUsername(String username);
}